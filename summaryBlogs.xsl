<?xml version="1.0" encoding="UTF-8"?>


<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                
            </head>
            <body> 
			<h1>
			Recent Blog Entries at http://gotoknow.org
			</h1>
                <table border = "1">
                    <tr bgcolor = "#9acd32">
                        <th>Title</th>
                        <th>Author</th>  
						<th>Link</th>  
                    </tr>
                    <xsl:for-each select="items/item">
                        <tr>
                            <td>
                                <xsl:value-of select="title"/>
                            </td>
								
                            <td>
                                <xsl:value-of select="author"/>
                            </td>
							<td>
							 <a>
                                    <xsl:attribute name="href"> 
                                    <xsl:value-of select="link"/> 
                                    </xsl:attribute>
                                <xsl:value-of select="link"/> 
								</a>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>                                
           
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
