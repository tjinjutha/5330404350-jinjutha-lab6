<?php

         header('Content-type: text/html; charset=utf-8');

         $url = "http://www.gotoknow.org/blogs/posts?format=rss";    

         $xml = file_get_contents($url);

         $xmlDoc = new DOMDocument();
         $xmlDocW = new DomDocument('1.0','UTF-8');

         $xmlDoc->loadXML($xml);
		 
		 echo "Reading from Recent Posts of website gotoknow.org<br/>";
			
		$root = $xmlDocW->appendChild($xmlDocW->createElement('items'));
				
         
		 $items = $xmlDoc->getElementsByTagName("item");

         for ($i = 0; $i < $items->length; $i++) {

                  $item = $items->item($i);

                  $titles = $item->getElementsByTagName("title");
				  $links = $item->getElementsByTagName("link");	
				  $authors = $item->getElementsByTagName("author");
				 
				
				 echo   $links->item(0)->nodeValue;
				 echo   $authors->item(0)->nodeValue;
				 
				 $titleText = $titles->item(0)->nodeValue;
				 $linkText = $links->item(0)->nodeValue;
				 $authorText = $authors->item(0)->nodeValue;
				 
	
		
		$itemE = $xmlDocW->createElement('item');
		$itemx = $root->appendChild($itemE);
		
		
		$titleE = $xmlDocW->createElement('title');
		$title = $itemx->appendChild($titleE);
		$title->appendChild( $xmlDocW->createTextNode($titleText));

		$linkE = $xmlDocW->createElement('link');
		$link = $itemx->appendChild($linkE);
		$link->appendChild( $xmlDocW->createTextNode($linkText));

		$authorE = $xmlDocW->createElement('author');
		$author = $itemx->appendChild($authorE);
		$author->appendChild( $xmlDocW->createTextNode($authorText));
		 	
		}

		$fileName = "summaryBlogs.xml";		 
		// save DOM tree in an XML file 
		$xmlDocW->formatOutput = true;
		$str = $xmlDocW->saveXML(); 
		$xmlDocW->save($fileName); 